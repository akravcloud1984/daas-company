<?php
namespace DaasSuite\Company\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\Serialize\Serializer\Json;
use DaasSuite\DaasIntegration\Helper\CustomerAccount as CustomerAccountHelper;
use DaasSuite\DaasIntegration\Helper\Address as DaasAddress;

class Data extends AbstractHelper
{
    /**
     * @var CustomerAccountHelper
     */
    private $customerAccount;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var DaasAddress
     */
    private $daasAddress;

    /**
     * @param Context $context
     * @param CustomerAccountHelper $customerAccount
     * @param Json $json
     * @param DaasAddress $daasAddress
     */
    public function __construct(
        Context $context,
        CustomerAccountHelper $customerAccount,
        Json $json,
        DaasAddress $daasAddress
    ) {
        $this->customerAccount = $customerAccount;
        $this->json = $json;
        $this->daasAddress = $daasAddress;

        parent::__construct($context);
    }

    /**
     * @param array $contactDetails
     * @return bool
     */
    public function detectCompany(array $contactDetails)
    {
        $isCompany = false;
        if ($daasCompanyFieldname = $this->getDaasCompanyFieldName()) {
            $daasCompanyField = trim($contactDetails[$daasCompanyFieldname] ?? '');
            $isCompany = !empty($daasCompanyField);
        }
        return $isCompany;
    }

    /**
     * @return string|bool
     */
    private function getDaasCompanyFieldName()
    {
        $daasFieldname = false;
        if ($mappingFields = $this->customerAccount->getMappingFields()) {
            $mappingArray = $this->json->unserialize($mappingFields);
            $daasFieldname = array_search($this->daasAddress->getMageField('company'), $mappingArray);
        }
        return $daasFieldname;
    }
}
