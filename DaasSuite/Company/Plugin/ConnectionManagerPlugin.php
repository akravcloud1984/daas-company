<?php
namespace DaasSuite\Company\Plugin;

use DaasSuite\DaasIntegration\Helper\ConnectionManager as Subject;
use DaasSuite\Company\Helper\Data as DefaultHelper;

class ConnectionManagerPlugin
{
    const EXPORT_CUSTOMER_ENDPOINT = '/api/contacts/new';

    /**
     * @var DefaultHelper
     */
    private $data;

    /**
     * @param DefaultHelper $data
     */
    public function __construct(
        DefaultHelper $data
    ) {
       $this->data = $data;
    }

    /**
     * @param Subject $subject
     * @param string $method
     * @param string $endpoint
     * @param array $params
     * @return array
     */
    public function beforeCreateRequest(Subject $subject, $method, $endpoint, $params)
    {
        if ($endpoint == self::EXPORT_CUSTOMER_ENDPOINT) {
            if ($this->data->detectCompany($params)) {
                // Space ' ' is being successfully saved in DAAS but empty string '' - not.
                $params['firstname'] = ' ';
                $params['lastname'] = ' ';
            }
        }

        return [$method, $endpoint, $params];
    }
}
